﻿<%@ Page Title="" Language="VB" MasterPageFile="~/DMT-Manager/MasterPagefor_money-transfer.master" AutoEventWireup="false" CodeFile="Send_Money.aspx.vb" Inherits="Send_Money" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    




    

    <style type="text/css">
        .bl_horizTable {
            border: 1px solid #ddd;
            width: 100%;
        }

            .bl_horizTable table {
                width: 100%;
            }

            .bl_horizTable th {
                width: 50%;
                padding: 15px;
                background-color: #efefef;
                border-bottom: 1px solid #ddd;
                font-weight: bold;
                vertical-align: middle;
            }

            .bl_horizTable td {
                padding: 15px;
                border-bottom: 1px solid #ddd;
            }

            .bl_horizTable tr:last-child th,
            .bl_horizTable tr:last-child td {
                border-bottom-width: 0;
            }

        @media screen and (max-width: 768px) {
            .bl_horizTable.bl_horizTable__mdScroll {
                border-right-width: 0;
                overflow-x: auto;
            }

                .bl_horizTable.bl_horizTable__mdScroll th,
                .bl_horizTable.bl_horizTable__mdScroll td {
                    white-space: nowrap;
                }

                .bl_horizTable.bl_horizTable__mdScroll td {
                    border-right: 1px solid #ddd;
                }
        }
    </style>

    <div id="content">

        <!-- Send Money
    ============================================= -->
        <section class="hero-wrap section shadow-md py-4">
            <div class="hero-mask opacity-7 bg-dark"></div>
            <div class="hero-bg" style="background-image: url('images/bg/image-6.jpg');"></div>
            <div class="hero-content py-5">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-xl-6 my-auto">
                            <div class="bg-white rounded shadow-md p-4">
                                <h3 class="text-5 text-center">Send Money</h3>
                                <hr class="mb-4">
                                <form id="form1" method="post">

                                    <div class="row">

                                        <div class="col-lg-6">
                                            <label for="recipientGets">Sender Mobile No.</label>
                                            <input type="text" class="form-control" data-bv-field="recipientGets" id="Text1" placeholder="Sender Mobile No." />
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="recipientGets"></label>
                                          
                                            <a href="#agent-register" data-toggle="modal" class="btn btn-primary btn-block" style="position: relative;top: 8px;">Search</a>
                                        </div>


                                        


                                    </div>
                                    <%--                                    <p class="text-muted mb-1">Total fees  - <span class="font-weight-500">7.21 USD</span></p>--%>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6 my-auto">

                            <div class="row no-gutters" style="background: #fff; border-radius: 5px;">
                                <div class="col-sm-5 d-flex justify-content-center bg-primary rounded-left py-4">
                                    <div class="my-auto text-center">
                                        <div class="text-17 text-white my-3"><i class="fas fa-user"></i></div>
                                        <%--                          <h3 class="text-4 text-white font-weight-400 my-3">Sanjeet Kunal</h3>--%>
                                        <%-- <div class="text-8 font-weight-500 text-white my-4">+91 8051293803</div>--%>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <h5 class="text-5 font-weight-400 m-3">Sender Details
                                    </h5>
                                    <hr>
                                    <div class="px-3">
                                        <ul class="list-unstyled">
                                            <li class="mb-2">
                                                <p class="text-muted mb-0">ID: FAS001</p>
                                            </li>
                                            <li class="mb-2">
                                                <p class="text-muted mb-0">Credit Limit: 25000</p>
                                            </li>
                                        </ul>

                                        <p class="d-flex align-items-center font-weight-500 mb-4">KYC Status<span class="text-3 ml-auto"><span class="bg-success text-white rounded-pill d-inline-block px-2 mb-0"> Completed</span></span></p>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Send Money End -->

        <section>
            <div class="bg-light shadow-sm rounded py-4 mb-4">
                <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-3">Beneficiary Details</h3>
                <!-- Title
            =============================== -->
                <div class="transaction-title py-2 px-4">
                    <div class="row">
                        <div class="col-1 col-sm-1 "><span class="text-4 font-weight-300">S NO.</span></div>
                        <div class="col col-sm-2"><span class="text-4">BENEFICIARY NAME</span></div>
                        <div class="col col-sm-2"><span class=" text-4">BANK ACCOUNT</span> </div>
                        <div class="col-auto col-sm-2 d-none d-sm-block "><span class=" text-4">BANK NAME</span></div>
                        <div class="col-3 col-sm-2 "><span class="text-4">IFSC CODE</span></div>
                        <div class="col-3 col-sm-2">STATUS</div>

                    </div>
                </div>
                <!-- Title End -->

                <!-- Transaction List
            =============================== -->
                <div class="transaction-list">
                    <div class="transaction-item px-4 py-3">
                        <div class="row align-items-center flex-row">
                            <div class="col-2 col-sm-1"><span class="d-block ">01</span></div>
                            <div class="col col-sm-2"><span class="d-block ">Sanjeet Kunal</span></div>
                            <div class="col col-sm-2"><span class="d-block">3675687876460</span></div>
                            <div class="col-auto col-sm-2 d-none d-sm-block"><span>State Bank of India</span> </div>
                            <div class="col-3 col-sm-2 "><span class="text-nowrap">SBIN0009004</span></div>
                            <div class="col-3 col-sm-2 ">Active</div>
                            <div class="col-1 col-sm-1"><span class="d-block "><a class="btn-link" data-toggle="collapse" href="#allFilters" aria-expanded="true" aria-controls="allFilters"><i class="fa fa-eye"></i></a></span></div>
                        </div>



                        <!--Beneficiary Details---Collapseable-->
                        <div class="collapse" id="allFilters">
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="bg-light shadow-sm rounded p-4 mb-4">
                                         <div class="bl_horizTable bl_horizTable__mdScroll">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <th>Beneficiary Name</th>

                                                        <td>Sanjeet Kunal</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Bank Account</th>
                                                        <td>330459487899</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Bank Name</th>
                                                        <td>State Bank Of India</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Beneficiary Mobile</th>
                                                        <td>8051293803</td>
                                                    </tr>
                                                    <tr>
                                                        <th>IFSC Code</th>
                                                        <td>SBIN0009004</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Varification Status</th>
                                                        <td>Verified</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>


                                        <br>
                                        <br>


                                        <div class="row">
                                            <div class="col-md-6">
                                                <button class="btn btn-primary btn-block"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete Beneficiary</button>
                                            </div>
                                            <div class="col-md-6">
                                                <%--  <button class="btn btn-success btn-block"><i class="fa fa-money"></i> &nbsp;&nbsp;Send Cash</button>--%>


                                                <a href="#edit-personal-details" data-toggle="modal" class="btn btn-success btn-block">Send Money</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Beneficiary Details---Collapseable---End-->
                    </div>

                </div>



                <div class="transaction-item px-4 py-3" data-target="#transaction-detail">
                    <div class="row align-items-center flex-row">
                        <div class="col-2 col-sm-1"><span class="d-block ">01</span></div>
                        <div class="col col-sm-2"><span class="d-block ">Sanjeet Kunal</span></div>
                        <div class="col col-sm-2"><span class="d-block">3675687876460</span></div>
                        <div class="col-auto col-sm-2 d-none d-sm-block"><span>State Bank of India</span> </div>
                        <div class="col-3 col-sm-2 "><span class="text-nowrap">SBIN0009004</span></div>
                        <div class="col-3 col-sm-2 ">Active</div>
                        <div class="col-1 col-sm-1"><span class="d-block "><i class="fa fa-eye"></i></span></div>
                    </div>
                </div>
                <div class="transaction-item px-4 py-3" data-target="#transaction-detail">
                    <div class="row align-items-center flex-row">
                        <div class="col-2 col-sm-1"><span class="d-block ">01</span></div>
                        <div class="col col-sm-2"><span class="d-block ">Sanjeet Kunal</span></div>
                        <div class="col col-sm-2"><span class="d-block">3675687876460</span></div>
                        <div class="col-auto col-sm-2 d-none d-sm-block"><span>State Bank of India</span> </div>
                        <div class="col-3 col-sm-2 "><span class="text-nowrap">SBIN0009004</span></div>
                        <div class="col-3 col-sm-2 ">Active</div>
                        <div class="col-1 col-sm-1"><span class="d-block "><i class="fa fa-eye"></i></span></div>
                    </div>
                </div>
                <div class="transaction-item px-4 py-3" data-target="#transaction-detail">
                    <div class="row align-items-center flex-row">
                        <div class="col-1 col-sm-1"><span class="d-block ">01</span></div>
                        <div class="col col-sm-2"><span class="d-block ">Sanjeet Kunal</span></div>
                        <div class="col col-sm-2"><span class="d-block">3675687876460</span></div>
                        <div class="col-auto col-sm-2 d-none d-sm-block"><span>State Bank of India</span> </div>
                        <div class="col-3 col-sm-2 "><span class="text-nowrap">SBIN0009004</span></div>
                        <div class="col-3 col-sm-2 ">Active</div>
                        <div class="col-1 col-sm-1"><span class="d-block "><i class="fa fa-eye"></i></span></div>
                    </div>
                </div>
                <div class="transaction-item px-4 py-3" data-target="#transaction-detail">
                    <div class="row align-items-center flex-row">
                        <div class="col-1 col-sm-1"><span class="d-block ">01</span></div>
                        <div class="col col-sm-2"><span class="d-block ">Sanjeet Kunal</span></div>
                        <div class="col col-sm-2"><span class="d-block">3675687876460</span></div>
                        <div class="col-auto col-sm-2 d-none d-sm-block"><span>State Bank of India</span> </div>
                        <div class="col-3 col-sm-2 "><span class="text-nowrap">SBIN0009004</span></div>
                        <div class="col-3 col-sm-2 ">Active</div>
                        <div class="col-1 col-sm-1"><span class="d-block "><i class="fa fa-eye"></i></span></div>
                    </div>
                </div>

                <%-- <a href="#"><span>+ Add More</span></a>--%>


                <div class="container">
               <%-- <a href="#" id="menu">+ Add More</a>--%>
                    <a href="#" data-toggle="collapse" data-target="#toggleCollapseOneAlternate" aria-expanded="false" aria-controls="toggleCollapseOneAlternate" class="collapsed">+ Add More</a>


                    <div id="toggleCollapseOneAlternate" class="collapse" aria-labelledby="toggleHeadingOneAlternate" style="">
                  <div class="card-body"> 
                      <div class=" row">

                      <div class="col-md-2">
                <select class="custom-select" id="input-zone" name="zone_id">
                            <option value=""> --- Select Bank --- </option>
                            <option value="3613">State Bank Of India</option>
                            <option value="3614">Axis Bank</option>
                            <option value="3615">ICICI Bank</option>
                            <option value="3616">Bank of India</option>
                            <option value="3617">HDFC Bank</option>
                            <option value="3618">Fedral Bank</option>
                            <option value="3619">DBS Bank</option>
                            <option value="3620">Punjab National Bank</option>
                       
                          </select>
                          </div>
                   <div class="col-md-2 col-xs-2">
                <input type="text" value="" class="form-control" data-bv-field="emailid" id="Text2" required="" placeholder="Bank Account No. *">
                          </div>
                   <div class="col-md-2 col-xs-2">
                <input type="text" value="" class="form-control" data-bv-field="emailid" id="Text3" required="" placeholder="IFSC Code *">
                          </div>
                      
           <div class="col-md-2">
                <input type="text" value="" class="form-control" data-bv-field="emailid" id="Text5" required="" placeholder="Beneficiary Name *">
                          </div>
                 <div class="col-md-2">
                <input type="text" value="" class="form-control" data-bv-field="emailid" id="Text4" required="" placeholder="Beneficiary Mobile No. *">
                          </div>
              <div class="col-md-2">
                         <button class="btn btn-primary btn-block">Submit</button>
                           </div>
            </div>
                     </div>
                </div>
        
          
             
           
 
                </div>


            </div>

           

        </section>






        <!-- Send Cash Popup--->

        <div id="edit-personal-details" class="modal fade " role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-400">Send Cash</h5>
                        <button type="button" class="close font-weight-400" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button>
                    </div>
                    <div class="modal-body p-4">
                        <form id="accountSettings" method="post">
                            <div class="bl_horizTable bl_horizTable__mdScroll">
                                <table>
                                    <tbody>
                                        <tr>
                                            <th>Beneficiary Name</th>
                                            <td>Sanjeet Kunal</td>
                                        </tr>
                                        <tr>
                                            <th>Bank Account</th>
                                            <td>330459487899</td>
                                        </tr>
                                        <tr>
                                            <th>Bank Name</th>
                                            <td>State Bank Of India</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-12 mb-3 collapse show" id="Div1" style="">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="allTransactions" name="allFilters" class="custom-control-input" checked="">
                                        <label class="custom-control-label" for="allTransactions">IMPS</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="paymentsSend" name="allFilters" class="custom-control-input">
                                        <label class="custom-control-label" for="paymentsSend">NEFT</label>
                                    </div>



                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control mt-2" placeholder="Enter Amount" />
                                </div>
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-primary btn-block mt-2" data-toggle="modal" data-target="#success_trans">Send </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Send Cash Popup End--->


     

    </div>

      


    


</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DMT-Manager/MasterPagefor_money-transfer.master" AutoEventWireup="true" CodeFile="SendMoney.aspx.cs" Inherits="InstantPay_SendMoney" %>

<%@ Register Src="~/DMT-Manager/uc_sendmoney.ascx" TagPrefix="uc1" TagName="uc_sendmoney" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="custom/css/validationcss.css" rel="stylesheet" />
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <style type="text/css">
        .bl_horizTable {
            border: 1px solid #ddd;
            width: 100%;
        }

            .bl_horizTable table {
                width: 100%;
            }

            .bl_horizTable th {
                width: 50%;
                padding: 15px;
                background-color: #efefef;
                border-bottom: 1px solid #ddd;
                font-weight: bold;
                vertical-align: middle;
            }

            .bl_horizTable td {
                padding: 15px;
                border-bottom: 1px solid #ddd;
            }

            .bl_horizTable tr:last-child th,
            .bl_horizTable tr:last-child td {
                border-bottom-width: 0;
            }

        @media screen and (max-width: 768px) {
            .bl_horizTable.bl_horizTable__mdScroll {
                border-right-width: 0;
                overflow-x: auto;
            }

                .bl_horizTable.bl_horizTable__mdScroll th,
                .bl_horizTable.bl_horizTable__mdScroll td {
                    white-space: nowrap;
                }

                .bl_horizTable.bl_horizTable__mdScroll td {
                    border-right: 1px solid #ddd;
                }
        }
    </style>

    <div id="content">
        <!-- Send Money    ============================================= -->
        <section class="hero-wrap section shadow-md py-4">
            <div class="hero-mask opacity-7 bg-dark"></div>
            <div class="hero-bg" style="background-image: url('images/bg/image-6.jpg');"></div>
            <div class="hero-content py-5">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-xl-6 my-auto">
                            <div class="bg-white rounded shadow-md p-4">
                                <h3 class="text-5 text-center">Search Sender Mobile Number</h3>
                                <hr class="mb-4" />
                                <div class="row">
                                    <div class="col-lg-6 form-validation">
                                        <%--<label for="recipientGets">Sender Mobile No.</label>--%>
                                        <input type="text" id="txtSenderMobileNo" value="" class="form-control sendmobileno" placeholder="Sender Mobile No." maxlength="10" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);" />
                                        <%--<asp:TextBox ID="txtSenderMobileNo" Text="" class="form-control sendmobileno" runat="server" placeholder="Sender Mobile No." MaxLength="10" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);"></asp:TextBox>--%>
                                    </div>
                                    <div class="col-lg-6">
                                        <%-- <label for="recipientGets"></label>--%>
                                        <span id="btnRemitterMobileSearch" style="cursor: pointer;" class="btn btn-primary btn-block" onclick="RemitterMobileSearch();">Search</span>
                                        <%--<asp:Button ID="MobileSearch" runat="server" class="btn btn-primary btn-block" Style="position: relative; top: 8px;" Text="Search" OnClick="MobileSearch_Click" OnClientClick="javascript: return CheckMobileNumber();" />--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6 my-auto" id="RemitterDetailsByMobile"></div>
                    </div>
                </div>
            </div>
        </section>

        <section id="ViewAllHistorySection" class="hidden">
            <div class="bg-light shadow-sm rounded py-4 mb-4">
                <div class="row">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-3">&nbsp;</div>
                            <div class="col-sm-4"><span id="btnViewAllBen" onclick="ViewAllBenDelClick();" style="cursor: pointer; float: right; margin-top: -10px;" class="btn btn-sm btn-primary btn-block"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;View All Beneficiary</span></div>
                            <div class="col-sm-4"><span id="btnViewAllTans" onclick="ViewAllTansClick();" style="cursor: pointer; float: right; margin-top: -10px;" class="btn btn-sm btn-primary btn-block"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;View Transaction History</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="BenificiarySection" class="hidden">
            <div class="col-sm-12">
                <div class="bg-light shadow-sm rounded py-4 mb-4">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-3">Beneficiary Details</h3>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-7">&nbsp;</div>
                                <div class="col-sm-4"><span id="btnAddBeneficiary" style="cursor: pointer; float: right; margin-top: -10px;" class="btn btn-sm btn-primary btn-block" onclick="AddNewBenDetail();"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add New Beneficiary</span></div>
                                <%--<div class="col-sm-4"><span id="btnViewTransHistory" style="cursor: pointer; float: right; margin-top: -10px;" class="btn btn-sm btn-primary btn-block"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;View Transaction History</span></div>--%>
                            </div>
                        </div>
                    </div>
                    <div class="transaction-title py-2 px-4">
                        <div class="row">
                            <div class="col-1 col-sm-1 "><span class="text-4 font-weight-300">S NO.</span></div>
                            <div class="col col-sm-2"><span class="text-4">BENEFICIARY NAME</span></div>
                            <div class="col col-sm-2"><span class=" text-4">BANK ACCOUNT</span> </div>
                            <div class="col-auto col-sm-2 d-none d-sm-block "><span class=" text-4">BANK NAME</span></div>
                            <div class="col-3 col-sm-2 "><span class="text-4">IFSC CODE</span></div>
                            <div class="col-3 col-sm-2"><span class="text-4">STATUS</span></div>
                        </div>
                    </div>
                    <div class="transaction-list">
                        <div class="px-4 py-3" id="BindBenificiary"></div>
                    </div>
                </div>
            </div>
        </section>

        <section id="AllTransHistoryDetail" class="hidden">
            <div class="shadow-sm rounded py-4 mb-4">
                <div class="col-lg-12">
                    <div class="row col-12 mb-3 collapse" id="allFilters">
                        <div class="col mb-2">
                            <div class="form-row">
                                <div class="col-sm-2 form-group">
                                    <input id="txtFilterFromDate" type="text" class="form-control CommanDate" placeholder="From Date" />
                                    <span class="icon-inside"><i class="fas fa-calendar-alt"></i></span>
                                </div>
                                <div class="col-sm-2 form-group">
                                    <input id="txtFilterToDate" type="text" class="form-control CommanDate" placeholder="To Date" />
                                    <span class="icon-inside"><i class="fas fa-calendar-alt"></i></span>
                                </div>
                                <div class="col-sm-3 form-group">
                                    <input id="txtFilterTransId" type="text" class="form-control" placeholder="Transaction Id" />                                   
                                </div>
                                <div class="col-sm-3 form-group">
                                    <input id="txtFilterTrackId" type="text" class="form-control" placeholder="Track Id" />                                   
                                </div> 
                                <div class="col-sm-2 form-group">
                                   <span id="btnTransFilterSearch" style="cursor: pointer;" class="btn btn-primary btn-md" onclick="TransFilterSearch();">Search</span>
                                    <span id="btnCancelTransFilterSearch" style="cursor: pointer;" class="btn btn-primary btn-md hidden" onclick="CancelTransFilterSearch();">X</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bg-light shadow-sm rounded py-4 mb-4">
                        <div class="row">
                            <div class="col-sm-6">
                                <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-3">All Transactions</h3>
                            </div>
                            <div class="col-sm-5 text-right">
                                <a class="btn-link collapsed" id="filtercollapsed" data-toggle="collapse" href="#allFilters" aria-expanded="false" aria-controls="allFilters">All Filters<i class="fas fa-sliders-h text-3 ml-1"></i></a>
                            </div>
                        </div>
                        <%-- <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-3">All Transactions</h3>--%>

                        <div class="transaction-title py-2 px-4">
                            <div class="row">
                                <div class="col col-sm-3">Description</div>
                                <div class="col col-sm-3">Account</div>
                                <div class="col col-sm-2">Amount</div>
                                <div class="col col-sm-1">Txn. Mode</div>
                                <div class="col-3 col-sm-3 text-center">Transaction Status</div>
                            </div>
                        </div>
                        <div id="BindTransDeails"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <uc1:uc_sendmoney runat="server" ID="uc_sendmoney" />

    <script src="custom/js/sendmoney.js"></script>
    <script src="custom/js/common.js"></script>
</asp:Content>


﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_sendmoney.ascx.cs" Inherits="DMT_Manager_uc_sendmoney" %>

<div class="modal fade" id="FormRegistration" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content" id="RegistSectionForm" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-body" style="padding-left: 20px!important;">
                <div class="container">
                    <div class="login-signup-page mx-auto">
                        <h3 class="font-weight-400 text-center">Sign Up for DMT
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                        </h3>
                        <p class="lead text-center">Your Sign Up information is safe with us.</p>
                        <div class="bg-light shadow-md rounded p-4 mx-2">
                            <div class="form-group form-validation">
                                <asp:TextBox ID="txtRegMobileNo" runat="server" class="form-control" placeholder="Enter Mobile No." MaxLength="10" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);"></asp:TextBox>
                            </div>
                            <div class="form-group form-validation">
                                <asp:TextBox ID="txtRegFirstName" runat="server" class="form-control" placeholder="Enter First Name"></asp:TextBox>
                            </div>
                            <div class="form-group form-validation">
                                <asp:TextBox ID="txtRegLastName" runat="server" class="form-control" placeholder="Enter Last Name"></asp:TextBox>
                            </div>
                            <div class="form-group form-validation">
                                <asp:TextBox ID="txtRegPinCode" runat="server" class="form-control" placeholder="Enter Area Pin Code" MaxLength="6" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);"></asp:TextBox>
                            </div>
                            <span id="btnRemtrRegistration" style="cursor: pointer;" class="btn btn-primary btn-block my-4" onclick="javascript: return CheckRegRemitter();">Register</span>
                            <p id="perrormessage" class="text-danger"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
            <div class="container hidden" id="OTPSectionForm">
                <div class="login-signup-page mx-auto">
                    <h3 class="font-weight-400 text-center text-success">
                        <button type="button" id="btnregformclose" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </h3>
                    <p class="lead text-center text-success">Otp sent successfully to your mobile number.</p>
                    <div class="bg-light shadow-md rounded p-4 mx-2">
                        <div class="form-group form-validation">
                            <label style="float: left; font-size: 1rem;">Enter OTP Number</label>
                            <input type="text" id="txtOtp" class="form-control" placeholder="Enter OTP Number." onkeypress="return isNumberValidationPrevent(event);" />
                        </div>
                        <span id="btnRegistrationVarification" style="cursor: pointer;" class="btn btn-primary btn-block my-4" onclick="javascript: return CheckRegRemitterVerification();">Verify</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-info btn-lg addbenificiarydetail hidden" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#FormBeneficiary"></button>
<div class="modal fade" id="FormBeneficiary" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-body" style="padding-left: 20px!important;">
                <div class="container" id="BeneficiarySectionForm">
                    <div class="login-signup-page mx-auto">
                        <h3 class="font-weight-400 text-center">New Beneficiary Detail
                                <button type="button" id="btnbenformclose" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                        </h3>
                        <div class="bg-light shadow-md rounded p-4 mx-2">
                            <%--  <div class="form-group form-validation">
                                    <select class="form-control" id="ddlBenBank">
                                        <option value="">--- Select Bank --- </option>
                                        <option value="State Bank Of India">State Bank Of India</option>
                                        <option value="Axis Bank">Axis Bank</option>
                                        <option value="ICICI Bank">ICICI Bank</option>
                                        <option value="Bank of India">Bank of India</option>
                                        <option value="HDFC Bank">HDFC Bank</option>
                                        <option value="Fedral Bank">Fedral Bank</option>
                                        <option value="DBS Bank">DBS Bank</option>
                                        <option value="Punjab National Bank">Punjab National Bank</option>
                                    </select>
                                </div>--%>
                            <div class="form-group form-validation">
                                <input id="txtBenAccountNo" type="text" class="form-control" placeholder="Enter Bank Account Number" onkeypress="return isNumberValidationPrevent(event);" />
                            </div>
                            <div class="form-group form-validation">
                                <input id="txtBenIFSCNo" type="text" class="form-control" placeholder="Enter IFSC Number" />
                            </div>
                            <div class="form-group form-validation">
                                <input id="txtBenPerName" type="text" class="form-control" placeholder="Enter Beneficiary Name" />
                            </div>
                            <div class="form-group form-validation">
                                <input id="txtBenPerMobile" type="text" class="form-control" placeholder="Enter Beneficiary Mobile Number" maxlength="10" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);" />
                            </div>
                            <span id="btnBenRegistration" style="cursor: pointer;" class="btn btn-primary btn-block my-4" onclick="javascript: return AddBeneficiaryDetail();">Submit</span>
                            <p id="benerrormessage" class="text-danger"></p>
                        </div>
                    </div>
                </div>
                <div class="container hidden" id="OTPBeneficiarySectionForm">
                    <div class="login-signup-page mx-auto">
                        <h3 class="font-weight-400 text-center text-success"><i class="fa fa-check-circle text-success" aria-hidden="true"></i>&nbsp;Success
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                        </h3>
                        <div class="bg-light shadow-md rounded p-4 mx-2">
                            <p class="lead text-center text-success">Beneficiary Added Successfully.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-info btn-lg moneytransfer hidden" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#ModelSendMoney"></button>
<div id="ModelSendMoney" class="modal fade " role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-400">Transfer Cash</h5>
                <button type="button" id="sendmoneypopup" class="close font-weight-400" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body p-4">
                <div class="bl_horizTable bl_horizTable__mdScroll">
                    <table>
                        <tbody>
                            <tr>
                                <th>Beneficiary Name</th>
                                <td><span id="SendMoneyBenName"></span></td>
                            </tr>
                            <tr>
                                <th>Bank Account</th>
                                <td><span id="SendMoneyBenAccNo"></span></td>
                            </tr>
                            <tr>
                                <th>Bank Name</th>
                                <td><span id="SendMoneyBenBankName"></span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-12 mb-3 collapse show" id="Div1" style="">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="allTransactions" name="sendtype" class="custom-control-input" value="IMPS" checked="" />
                            <label class="custom-control-label" for="allTransactions">IMPS</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="paymentsSend" name="sendtype" class="custom-control-input" value="NEFT" />
                            <label class="custom-control-label" for="paymentsSend">NEFT</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-validation">
                        <input type="text" class="form-control mt-2" id="txtBenAmount" placeholder="Enter Amount" onkeypress="return isNumberValidationPrevent(event);" />
                    </div>
                    <div class="col-md-6">
                        <span id="btnBenSendMoney" style="cursor: pointer;" class="btn btn-primary btn-block mt-2" onclick="javascript: return BenSendMoneyEvent();">Transfer</span>
                    </div>
                </div>
                <div class="row"><span class="text-danger" id="errortranfermoney"></span></div>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-info btn-lg successmessage hidden" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#SuccessMsg"></button>
<div class="modal fade" id="SuccessMsg" role="dialog">
    <div class="modal-dialog modal-md" style="margin: 15% auto!important;">
        <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-header" style="padding: 12px;">
                <h5 class="modal-title modelheading"></h5>
            </div>
            <div class="modal-body" style="padding-left: 20px!important;">
                <h6 class="sucessmsg"></h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-info btn-lg hidden delbendetail" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#DeleteBenDetail">hello</button>
<div class="modal fade" id="DeleteBenDetail" role="dialog">
    <div class="modal-dialog modal-md" style="margin: 10% auto!important;">
        <div id="ConfirmBenDelete" class="modal-content" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-body" style="padding: 30px!important;">
                <h6>Are you sure want to delete this benificiary detail?</h6>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnDelConfirmed" class="btn btn-sm btn-success" style="padding: 0.5rem 1rem!important;" onclick="ConfirmDeleteDen();">YES</button>
                <button type="button" class="btn btn-sm btn-danger" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">NO</button>
            </div>
        </div>
        <div id="BenDeleteOtpSent" class="modal-content hidden" style="text-align: center; border-radius: 5px !important;">
            <div class="container">
                <div class="login-signup-page mx-auto">
                    <h3 class="font-weight-400 text-center text-success">
                        <button type="button" id="BenDelCloseClick" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </h3>
                    <p class="lead text-center text-success">Otp sent successfully to your mobile number.</p>
                    <div class="bg-light shadow-md rounded p-4 mx-2">
                        <div class="form-group form-validation">
                            <label style="float: left; font-size: 1rem;">Enter OTP Number</label>
                            <input type="text" id="txtBenDeleteOtp" class="form-control" placeholder="Enter OTP Number." onkeypress="return isNumberValidationPrevent(event);" />
                        </div>
                        <span id="btnBenDelVarification" style="cursor: pointer;" class="btn btn-primary btn-block my-4" onclick="javascript: return BenDelVarification();">Verify</span>
                        <p id="bendelvariyerror" class="text-danger"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
        <%--var mobilenoid = "<%=txtSenderMobileNo.ClientID%>";
        var searchbtnid = "<%=MobileSearch.ClientID%>";--%>
        var regmobile = "<%=txtRegMobileNo.ClientID%>";
        var regfirstname = "<%=txtRegFirstName.ClientID%>";
        var reglastname = "<%=txtRegLastName.ClientID%>";
        var regpincode = "<%=txtRegPinCode.ClientID%>";
        //var regbutton = "<//%=btnRemtrRegistration.ClientID%>";
</script>

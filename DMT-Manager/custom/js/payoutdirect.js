﻿$(function () {
    $("#txtPayoutSenderMobileNo").val("");
    $("#btnPayoutRemitterMobileSearch").html("Search").prop('disabled', false);
    $("#PayoutTokenExpired").html("");
});

function PayoutRemitterMobileSearch() {
    alert();
    $("#PayoutTokenExpired").html("");
    var thisbutton = $("#btnPayoutRemitterMobileSearch");
    if (CheckFocusBlankValidation("txtPayoutSenderMobileNo")) return !1;
    var mobileno = $("#txtPayoutSenderMobileNo").val();

    if (mobileno.length == 10) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/dmt-manager/senderindex.aspx/remittermobilesearch",
            data: '{mobileno: ' + JSON.stringify(mobileno) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d.length > 0) {
                    if (data.d[0] == "success") { window.location.href = data.d[1]; }
                    else if (data.d[0] == "otpsent" || data.d[0] == "registration") {
                        window.location.href = data.d[1];
                    }
                    else if (data.d[0] == "error") {
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Failed !", "#ff414d", data.d[1], "#ff414d");
                        $(thisbutton).html("Search");
                    }
                    else if (data.d[0] == "reload") { window.location.reload(); }
                }
                else {
                    $("#PayoutTokenExpired").html("Error occured, Token expired !");
                }
                $(thisbutton).html("Search");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
    else {
        ShowMessagePopup("<i class='fa fa-exclamation-triangle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Failed !", "#ff414d", "Mobile number not valid.", "#ff414d");
    }
}

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".popupopenclass").click();
    $(".popupheading").css("color", headcolor).html(headerHeading);
    $(".popupcontent").css("color", bodycolor).html(bodyMsg);
}